<?php
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 2/14/2020
 * Time: 8:55 PM
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once "vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(__DIR__.'/Entities');
$isDevMode = false;

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'host'      => '188.240.210.8',
    'password' => 'scoalait123',
    'dbname'   => 'web_04_iulia1',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
$entityManager = EntityManager::create($dbParams, $config);


// replace with mechanism to retrieve EntityManager in your app
//$entityManager = GetEntityManager();

return  ConsoleRunner::createHelperSet($entityManager);
