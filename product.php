<?php include 'functions.php';
$categories=$entityManager->getRepository(\Entities\Category::class)->findAll();
$product= $entityManager->getRepository(\Entities\Product::class)->findOneBy(['id'=>$_GET['id']]);

echo $twig->render('product.html.twig',['product'=>$product,'categories'=>$categories]);
?>