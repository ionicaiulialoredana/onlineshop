<?php
include "vendor/autoload.php";
session_start();

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(__DIR__."/Entities");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'host'      => '188.240.210.8',
    'password' => 'scoalait123',
    'dbname'   => 'web_04_iulia1',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);

$entityManager = EntityManager::create($dbParams, $config);

$loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/Templates');
$twig = new \Twig\Environment($loader, [
 'cache' => !$isDevMode,
]);


function isAuthenticated()
{
    if (!isset($_SESSION['username'])){
        header('Location: login.php');
        die;
    }
}