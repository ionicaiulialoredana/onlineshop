<?php include "functions.php";
$categories=$entityManager->getRepository(\Entities\Category::class)->findAll();
$products = $entityManager->getRepository(\Entities\Product::class)->findAll();

echo $twig->render('index.html.twig',['categories'=>$categories,'products'=>$products]);

?>